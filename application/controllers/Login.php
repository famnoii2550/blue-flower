<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function register()
    {
        $session = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
        if ($session) {
            redirect('my-profile');
        } else {
            $this->load->view('register');
        }
    }

    public function register_success()
    {
        $frist_name             = $this->input->post('frist_name');
        $last_name              = $this->input->post('last_name');
        $username               = $this->input->post('username');
        $password               = $this->input->post('password');
        $c_password             = $this->input->post('c_password');
        $get_user               = $this->Login_model->get_user($username);

        if (empty($password || $c_password)) {
            echo "<script>";
            echo "alert('กรุณากรอกข้อมูล Password ให้ตรงกัน');";
            echo "window.location='register'";
            echo "</script>";
        } elseif ($password != $c_password) {
            echo "<script>";
            echo "alert('กรุณากรอกข้อมูล Password ให้ตรงกัน');";
            echo "window.location='register'";
            echo "</script>";
        } elseif ($get_user) {
            echo "<script>";
            echo "alert('Username นี้มีผู้อื่นใช้แล้ว กรุณาลองใหม่อีกครั้ง !!!');";
            echo "window.location='register'";
            echo "</script>";
        } else {
            $data = array(
                'frist_name'        => $frist_name,
                'last_name'         => $last_name,
                'username'          => $username,
                'password'          => md5($password),
                'createdDtm'        => date('Y-m-d H:i:s')
            );
            if ($this->db->insert('tb_user', $data)) {
                echo "<script>";
                echo "alert('ท่านได้สมัครสมาชิกเรียบร้อยแล้ว สามารถเข้าสู่ระบบได้ทันทีค่ะ');";
                echo "window.location='login'";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาดในการสมัครสมาชิก กรุณาลองใหม่อีกครั้งภายหลัง !!');";
                echo "window.location='register'";
                echo "</script>";
            }
        }
    }

    public function login()
    {

        $session = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
        if ($session) {
            redirect('my-profile');
        } else {
            $this->load->view('sign-in');
        }
    }

    public function loginMe()
    {
        $this->load->library('form_validation');
        //1. เปลี่ยนชื่อ name ของ input
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            //2. กำหนดตัวแปรให้มีค่า = name ของ input (ตั้งตัวแปรเป็นชื่ออะไรก็ได้)
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            //3. ส่งค่าตัวแปรที่กำหนดไว้ ไปเช็คใน Database ที่ model
            if ($this->Login_model->login($username, $password)) {
                //4. เปรียบเทียบระหว่าง ฟิลใน database => ตัวแปรที่กำหนด
                $session_data = array(
                    'username' => $username
                );
                //5. ถ้าใช่ให้เก็บค่าตัวแปรไว้ใน $session_data และไปหน้าที่กำหนด (redirect)
                $this->session->set_userdata($session_data);
                //6. ถ้ามีการ LOGIN เข้ามาให้ insert เวลาLOGIN เข้า Database
                if ($session_data > 1) {
                    $check = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
                    //7. กำหนดตัวแปรที่ต้องการจะ insert
                    $data = array(
                        'id_user'       => $check['userId'],
                        'createdDtm'    => date('Y-m-d H:i:s')
                    );
                    //8. Insert ข้อมูลเข้า Database
                    $this->db->insert('tb_session', $data);
                }
                if ($session_data > 1) {
                    echo "<script>";
                    echo "alert('ยินเีต้อนรับเข้าสู่ระบบ ขอให้ช็อปอย่างมีความสุขค่ะ');";
                    echo "window.location='index'";
                    echo "</script>";
                }
            } else {
                $this->session->set_flashdata('error', '<i class="fa fa-exclamation-triangle"></i> ชื่อผู้ใช้งาน หรือ รหัสผ่าน ไม่ถูกต้อง !!');
                redirect('sign-in', 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', '<i class="fa fa-exclamation-triangle"></i> เกิดข้อผิดพลาดในการเข้าสู่ระบบ กรุณาลองใหม่อีกครั้ง !!');
            redirect('sign-in');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy(); //ล้างsession

        echo "<script>";
        echo "alert('ท่านได้ออกจากระบบเรียบร้อยแล้ว');";
        echo "window.location='sign-in'";
        echo "</script>";
    }
}
