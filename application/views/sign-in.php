<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blue Flower</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- ดึงไฟล์ style มาใช้ -->
    <?php include('include/style.php'); ?>
    <!-- สิ้นสุดการดึงไฟล์ style มาใช้ -->
</head>

<body class="goto-here">
    <!-- ดึงไฟล์ menu มาใช้ -->
    <?php include('include/menu.php'); ?>
    <!-- สิ้นสุดการดึงไฟล์ menu มาใช้ -->
    <div class="hero-wrap hero-bread" style="background-image: url('assets/frontend/images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index">Home</a></span> <span>เข้าสู่ระบบ</span></p>
                    <h1 class="mb-0 bread">เข้าสู่ระบบ</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-12 ftco-animate">
                    <form action="loginMe" method="post" class="billing-form">
                        <h3 class="mb-4 billing-heading">เข้าสู่ระบบ</h3>
                        <?php if ($error = $this->session->flashdata('error')) : ?>
                            <p style="color:red;text-align:center;"><?php echo $error; ?></p>
                        <?php endif; ?>
                        <div class="row align-items-end">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="username">ชื่อผู้ใช้งาน / Username</label>
                                    <input type="text" class="form-control" placeholder="ชื่อผู้ใช้งาน" name="username" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstname">รหัสผ่าน / Password</label>
                                    <input type="password" class="form-control" placeholder="รหัสผ่าน" name="password" required>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success">เข้าสู่ระบบ</button>
                            </div>
                            <div class="col-md-12 text-center" style="margin:10px">
                                <p>ท่านได้เป็นสมาชิกอยู่แล้วหรือไม่? <a href="register"><span><u>สมัครสมาชิก</u></span></a> ได้เลยที่นี่</p>
                            </div>
                        </div>
                    </form>
                    <!-- END -->
                </div>
            </div>
        </div>
    </section> <!-- .section -->

    <?php include('include/footer.php'); ?>
    
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
    </div>
    <?php include('include/script.php'); ?>
    <script>
        $(document).ready(function() {

            var quantitiy = 0;
            $('.quantity-right-plus').click(function(e) {

                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                $('#quantity').val(quantity + 1);


                // Increment

            });

            $('.quantity-left-minus').click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                // Increment
                if (quantity > 0) {
                    $('#quantity').val(quantity - 1);
                }
            });

        });
    </script>

</body>

</html>