<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blue Flower</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- ดึงไฟล์ style มาใช้ -->
    <?php include('include/style.php'); ?>
    <!-- สิ้นสุดการดึงไฟล์ style มาใช้ -->
</head>

<body class="goto-here">
    <!-- ดึงไฟล์ menu มาใช้ -->
    <?php include('include/menu.php'); ?>
    <!-- สิ้นสุดการดึงไฟล์ menu มาใช้ -->
    <div class="hero-wrap hero-bread" style="background-image: url('assets/frontend/images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index">หน้าแรก</a></span> <span>สมัครสมาชิก</span></p>
                    <h1 class="mb-0 bread">สมัครสมาชิก</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-12 ftco-animate">
                    <form action="register_success" method="post" class="billing-form">
                        <h3 class="mb-4 billing-heading">สมัครสมาชิก</h3>
                        <div class="row align-items-end">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">ชื่อจริง</label>
                                    <input type="text" class="form-control" placeholder="" name="frist_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">นามสกุล</label>
                                    <input type="text" class="form-control" placeholder="" name="last_name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="username">ชื่อผู้ใช้งาน / Username</label>
                                    <input type="text" class="form-control" placeholder="" name="username">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">รหัสผ่าน / Password</label>
                                    <input type="password" class="form-control" placeholder="" name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">ยืนยัน / Re-Password</label>
                                    <input type="password" class="form-control" placeholder="" name="c_password">
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success">สมัครสมาชิก</button>
                            </div>
                            <div class="col-md-12 text-center" style="margin:10px">
                                <p>ท่านได้เป็นสมาชิกอยู่แล้วหรือไม่? <a href="sign-in"><span><u>เข้าสู่ระบบ</u></span></a> ได้เลยที่นี่</p>
                            </div>
                        </div>
                    </form>
                    <!-- END -->
                </div>
            </div>
        </div>
    </section> <!-- .section -->

    <?php include('include/footer.php'); ?>
    
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
    </div>
    <?php include('include/script.php'); ?>
    <script>
        $(document).ready(function() {

            var quantitiy = 0;
            $('.quantity-right-plus').click(function(e) {

                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                $('#quantity').val(quantity + 1);


                // Increment

            });

            $('.quantity-left-minus').click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('#quantity').val());

                // If is not undefined

                // Increment
                if (quantity > 0) {
                    $('#quantity').val(quantity - 1);
                }
            });

        });
    </script>

</body>

</html>